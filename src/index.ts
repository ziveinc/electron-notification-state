import {
  getNotificationState as getWindowsNotificationState,
  QUERY_USER_NOTIFICATION_STATE,
} from 'windows-notification-state'
import { getFocusAssist } from 'windows-focus-assist'
import {
  getSessionState as getDarwinSessionState,
  getDoNotDisturb as getDarwinDoNotDisturb,
  SessionState,
} from 'macos-notification-state'

export type NotificationState =
  | QUERY_USER_NOTIFICATION_STATE
  | SessionState
  | 'UNKNOWN'
  | 'UNKNOWN_ERROR'
  | 'DO_NOT_DISTURB'

export function getSessionState(): NotificationState {
  if (process.platform === 'win32') {
    return getWindowsNotificationState()
  }

  if (process.platform === 'darwin') {
    return getDarwinSessionState()
  }

  return 'UNKNOWN_ERROR'
}

export function getDoNotDisturb(): boolean {
  if (process.platform === 'win32') {
    const focusAssist = getFocusAssist();
    return focusAssist.value > 0;
  }

  if (process.platform === 'darwin') {
    return getDarwinDoNotDisturb()
  }

  return false
}
