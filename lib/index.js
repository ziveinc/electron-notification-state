"use strict";
exports.__esModule = true;
exports.getDoNotDisturb = exports.getSessionState = void 0;
var windows_notification_state_1 = require("windows-notification-state");
var windows_focus_assist_1 = require("windows-focus-assist");
var macos_notification_state_1 = require("macos-notification-state");
function getSessionState() {
    if (process.platform === 'win32') {
        return (0, windows_notification_state_1.getNotificationState)();
    }
    if (process.platform === 'darwin') {
        return (0, macos_notification_state_1.getSessionState)();
    }
    return 'UNKNOWN_ERROR';
}
exports.getSessionState = getSessionState;
function getDoNotDisturb() {
    if (process.platform === 'win32') {
        var focusAssist = (0, windows_focus_assist_1.getFocusAssist)();
        return focusAssist.value > 0;
    }
    if (process.platform === 'darwin') {
        return (0, macos_notification_state_1.getDoNotDisturb)();
    }
    return false;
}
exports.getDoNotDisturb = getDoNotDisturb;
